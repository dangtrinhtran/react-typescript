import * as React from 'react';
import { Route, Switch, RouteComponentProps } from "react-router-dom";
import NotificationSystem from "react-notification-system";

import routes from "../routes";
import Footer from "../components/Footer/Footer";
import SideBar from "../components/SideBar/SideBar";
import NavBars from "../components/NavBars/NavBars";

import { style } from "../variables/Variables"

import image from "../assets/img/sidebar-3.jpg";

interface MainState {
    image: string;
    color: string;
    hasImage: boolean;
    _notificationSystem: any;
}

class Main extends React.Component<RouteComponentProps, MainState> {

    public notifyRef: any;

    constructor(props: RouteComponentProps) {
        super(props);
        this.notifyRef = React.createRef();
        this.state = {
            _notificationSystem: null,
            image: image,
            color: 'black',
            hasImage: false,
        };
    }

    public getRoutes = (routes: any) => {
        return routes.map((prop: any, key: any) => {
            if (prop.layout === '/admin') {
                return (
                    <Route
                        path={prop.layout + prop.path}
                        render={props => (
                            <prop.component
                                {...props}
                                handleClick={this.handleNotificationClick}
                            />
                        )}
                        key={key}
                    />
                );
            } else {
                return null;
            }
        });
    };

    public handleNotificationClick = (position: any) => {
        const color = Math.floor(Math.random() * 4 + 1);
        let level;
        switch (color) {
            case 1:
                level = 'success';
                break;
            case 2:
                level = 'warning';
                break;
            case 3:
                level = 'error';
                break;
            case 4:
                level = 'info';
                break;
            default:
                break;
        }

        this.state._notificationSystem.addNotification({
            title: <span data-notify="icon" className="pe-7s-gift" />,
            message: (
                <div>
                    Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for
                    every web developer.
                </div>
            ),
            level: level,
            position: position,
            autoDismiss: 15
        });
    };

    public componentDidMount(): void {
        this.setState({
            _notificationSystem: this.notifyRef.notificationSystem,
        });
        let _notificationSystem = this.notifyRef.notificationSystem;
        let color = Math.floor(Math.random() * 4 + 1);
        let level;
        switch (color) {
            case 1:
                level = 'success';
                break;
            case 2:
                level = 'warning';
                break;
            case 3:
                level = 'error';
                break;
            case 4:
                level = 'info';
                break;
            default:
                break;
        }
        if (_notificationSystem) {
            _notificationSystem.addNotification({
                title: <span data-notify="icon" className="pe-7s-gift"/>,
                message: (
                    <div>
                        Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for
                        every web developer.
                    </div>
                ),
                level: level,
                position: 'tr',
                autoDismiss: 15
            });
        }
    }

    public getBrandText = (path: string) => {
        for (let i = 0; i < routes.length; i++) {
            if (
                this.props.location.pathname.indexOf(
                    routes[i].layout + routes[i].path
                ) !== -1
            ) {
                return routes[i].name;
            }
        }

        return 'Brand';
    };

    public render() {
        return (
            <div className="wrapper">
                <NotificationSystem ref="notificationSystem" style={false} />
                <SideBar {...this.props} routes={routes} image={this.state.image}
                         color={this.state.color}
                         hasImage={this.state.hasImage}/>
                <div id="main-panel" className="main-panel" ref="mainPanel">
                    <NavBars
                        {...this.props}
                        brandText={this.getBrandText(this.props.location.pathname)}
                    />
                    <Switch>{this.getRoutes(routes)}</Switch>
                    <Footer />
                </div>
            </div>
        );
    }
}

export default Main;
