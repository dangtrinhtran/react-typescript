import * as React from 'react';
import './App.css';
import {Switch, Route, withRouter, RouteComponentProps, Redirect, Link} from 'react-router-dom';
import List from '../../components/Customer/List';
import Create from '../../components/Customer/Create';
import Edit from '../../components/Customer/Edit';
import Pwa from '../../components/Magento2/Pwa';
import Report from '../../components/Chart/Report';
import Demo from "../../components/Demo/Demo";

import Main from "../../layouts/Main";


class App extends React.Component<RouteComponentProps<any>> {
    public render() {
        /*return (
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to={'/'}> List </Link>
                        </li>

                        <li>
                            <Link to={'/create'}> Create Customer </Link>
                        </li>

                        <li>
                            <Link to={'/pwa'}> PWA </Link>
                        </li>

                        <li>
                            <Link to={'/report'}> Chart </Link>
                        </li>

                        <li>
                            <Link to={'/demo'}> Demo </Link>
                        </li>
                    </ul>
                </nav>

                <Switch>
                    <Route path={'/'} exact component={List}/>
                    <Route path={'/create'} exact component={Create}/>
                    <Route path={'/edit/:id'} exact component={Edit}/>
                    <Route path={'/pwa'} exact component={Pwa}/>
                    <Route path={'/report'} exact component={Report}/>
                    <Route path={'/demo'} exact component={Demo}/>
                </Switch>
            </div>
        );*/
        return (
            <Switch>
                <Route path="/admin" render={props => <Main {...props} />} />
                <Redirect from="/" to="/admin/dashboard" />
            </Switch>
        );
    }
}

export default withRouter(App);
