import Dashboard from "./views/Dashboard";
import Magento2 from "./views/Magento2";
import DemoReact from "./views/DemoReact";
import Detail from "./components/Magento2/Detail";

const dashboardRoutes = [
    {
        path: "/dashboard",
        name: "Dashboard",
        icon: "pe-7s-graph",
        component: Dashboard,
        layout: "/admin",
    },
    {
        path: "/magento2",
        name: "Magento 2",
        icon: "pe-7s-news-paper",
        component: Magento2,
        layout: "/admin"
    },
    {
        path: "/demo",
        name: "Demo React",
        icon: "pe-7s-note2",
        component: DemoReact,
        layout: "/admin"
    },
    {
        path: "/categories/:id",
        name: "Categories",
        icon: "pe-7s-news-paper",
        component: Detail,
        layout: "/admin"
    }
];

export default dashboardRoutes;
