import * as React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import { Grid, Row, Col } from "react-bootstrap";
import NavigationMenu from "../components/Magento2/NavigationMenu";

class Magento2 extends React.Component {

    public render() {
        return (
            <div className="content">
                <NavigationMenu />
            </div>
        );
    }
}

export default Magento2;
