import * as React from 'react';
import {RouteComponentProps} from 'react-router-dom';
import { Grid, Row, Col } from "react-bootstrap";
import {Data} from "../components/Chart/Data";
import BarChart from "../components/Chart/BarChart";
import LineChart from "../components/Chart/LineChart";
import ChartWrapper from "../components/Chart/ChartWrapper";

interface DataValues {
    value: any;
}

class Dashboard extends React.Component<RouteComponentProps, DataValues> {

    public chartData: Data;

    public intervalID: number;

    constructor(props: RouteComponentProps) {
        super(props);
        this.chartData = new Data();
        this.intervalID = 0;
        this.state = {
            value: this.chartData.getData(),
        };
    }

    public componentDidMount(): void {
        this.intervalID = window.setInterval(() => {
            this.setState({
                value: this.chartData.getData(),
            });
        }, 5000);
        console.log('did mount');
    }

    public componentWillUnmount(): void {
        clearInterval(this.intervalID);
        console.log('will unmount');
    }

    public render() {
        console.log('render');
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col lg={12} sm={12}>
                            <div className="chart-wrapper">
                                <ChartWrapper
                                    statsIcon="fa fa-history"
                                    title={this.state.value[0].title}
                                    category="24 Hours performance"
                                    stats="Updated 5 seconds ago"
                                    content={
                                        <div className="ct-chart">
                                            <LineChart
                                                data={this.state.value[0].data}
                                                title={this.state.value[0].title}
                                                color="#3E517A"
                                            />
                                        </div>
                                    }
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div className="chart-wrapper">
                                <ChartWrapper
                                    statsIcon="fa fa-history"
                                    title={this.state.value[1].title}
                                    category="24 Hours performance"
                                    stats="Updated 5 seconds ago"
                                    content={
                                        <div className="ct-chart">
                                            <BarChart
                                                data={this.state.value[1].data}
                                                title={this.state.value[1].title}
                                                color="#70CAD1"
                                            />
                                        </div>
                                    }
                                />
                            </div>
                        </Col>
                    </Row>

                </Grid>
            </div>
        );
    }
}

export default Dashboard;
