import * as React from 'react';
import Chart from 'chart.js';

interface BarChartProps {
    data: any,
    title: string,
    color: string,
    //color?: string, //? for optional
}

class BarChart extends React.Component<BarChartProps> {

    public canvasRef: any;

    public myChart: any;

    constructor(props: BarChartProps) {
        super(props);

        this.canvasRef = React.createRef();
    }

    public static defaultProps: Partial<BarChartProps> = {
        data: {},
        title: '',
        color: '',
    };

    public componentDidUpdate(): void {
        this.myChart.data.labels = this.props.data.map((d: any) => d.label);
        this.myChart.data.datasets[0].data = this.props.data.map((d: any) => d.value);
        this.myChart.update();
    }

    public componentDidMount(): void {

        this.myChart = new Chart(this.canvasRef.current, {
            type: 'bar',
            data: {
                labels: this.props.data.map((d: any) => d.label),
                datasets: [
                    {
                        label: this.props.title,
                        data: this.props.data.map((d: any) => d.value),
                        backgroundColor: this.props.color,
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                min: 0,
                                max: 100
                            }
                        }
                    ]
                }
            }
        });
    }

    public render() {
        return <canvas ref={this.canvasRef} />;
    }
}

export default BarChart;
