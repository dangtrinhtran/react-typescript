/**
 * Class Data
 */
export class Data
{
    // Data generation
    public getRandomArray(numItems: number)
    {
        // Create random array of objects
        let names = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let data = [];
        for(let i = 0; i < numItems; i++) {
            data.push({
                label: names[i],
                value: Math.round(20 + 80 * Math.random())
            });
        }

        return data;
    }

    public getRandomDateArray(numItems: number)
    {
        // Create random array of objects (with date)
        let data = [];
        let baseTime = new Date().getTime();
        let dayMs = 24 * 60 * 60 * 1000;
        for (let i = 0; i < numItems; i++) {
            data.push({
                time: new Date(baseTime + i * dayMs).toDateString(),
                value: Math.round(20 + 80 * Math.random())
            });
        }
        
        return data;
    }

    public getData(): object[] {
        let data = [];

        data.push({
            title: 'Visits',
            data: this.getRandomDateArray(150),
        });

        data.push({
            title: 'Categories',
            data: this.getRandomArray(20),
        });

        data.push({
            title: 'Categories',
            data: this.getRandomArray(10),
        });

        data.push({
            title: 'Data 4',
            data: this.getRandomArray(6),
        });

        return data;
    }

}
