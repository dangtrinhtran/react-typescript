import * as React from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import {Data} from './Data';
import LineChart from "./LineChart";
import BarChart from "./BarChart";
import Footer from "../Footer/Footer";

interface DataValues {
    value: any;
}

class Report extends React.Component<RouteComponentProps, DataValues> {

    public chartData: Data;

    private checkData: Data;

    constructor(props: RouteComponentProps, checkData: Data) {
        super(props);
        this.chartData = new Data();
        this.checkData = checkData;
        this.state = {
            value: this.chartData.getData(),
        };
    }

    public componentDidMount(): void {
        window.setInterval(() => {
            this.setState({
                value: this.chartData.getData(),
            });
        }, 5000);

    }

    public render() {
        //console.log(this.checkData.getData());
        return (
            <div className="wrapper">
                <div className="content">
                    <div className="container-fluid">
                        <div className="chart-wrapper">
                            <LineChart
                                data={this.state.value[0].data}
                                title={this.state.value[0].title}
                                color="#3E517A"
                            />
                        </div>
                        <div className="chart-wrapper">
                            <BarChart
                                data={this.state.value[1].data}
                                title={this.state.value[1].title}
                                color="#70CAD1"
                            />
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default withRouter(Report)
