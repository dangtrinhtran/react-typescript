import * as React from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import Chart from 'chart.js';

interface LineChartProps {
    data: any,
    title: string,
    color: string,
    //color?: string, //? for optional
}

class LineChart extends React.Component<LineChartProps> {

    public canvasRef: any;

    public myChart: any;

    constructor(props: LineChartProps) {
        super(props);

        this.canvasRef = React.createRef();
    }

    public static defaultProps: Partial<LineChartProps> = {
        data: {},
        title: '',
        color: '',
    };

    public componentDidUpdate(): void {
        this.myChart.data.labels = this.props.data.map((d: any) => d.time);
        this.myChart.data.datasets[0].data = this.props.data.map((d: any) => d.value);
        this.myChart.update();
    }

    public componentDidMount(): void {

        this.myChart = new Chart(this.canvasRef.current, {
            type: 'line',
            data: {
                labels: this.props.data.map((d: any) => d.time),
                datasets: [
                    {
                        label: this.props.title,
                        data: this.props.data.map((d: any) => d.value),
                        fill: false,//'none'
                        backgroundColor: this.props.color,
                        pointRadius: 2,
                        borderColor: this.props.color,
                        borderWidth: 1,
                        lineTension: 0
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                scales: {
                    xAxes: [
                        {
                            type: 'time',
                            time: {
                                unit: 'week'
                            }
                        }
                    ],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    public render() {
        return <canvas ref={this.canvasRef} />;
    }
}

export default LineChart;
