import * as React from 'react';

interface ChartWrapperProps {
    plain?: boolean;
    hCenter?: string;
    title: string;
    category: string;
    ctAllIcons?: boolean;
    ctTableFullWidth?: boolean;
    ctTableResponsive?: boolean;
    content: any;
    stats?: string;
    statsIcon: string;
}

class ChartWrapper extends React.Component<ChartWrapperProps> {

    public static defaultProps: Partial<ChartWrapperProps> = {
        plain: false,
        hCenter: '',
        title: '',
        category: '',
        ctAllIcons: false,
        ctTableFullWidth: false,
        ctTableResponsive: false,
        content: {},
        stats: '',
        statsIcon: '',
    };

    public render() {
        return (
            <div className={"card" + (this.props.plain ? " card-plain" : "")}>
                <div className={"header" + (this.props.hCenter ? " text-center" : "")}>
                    <h4 className="title">{this.props.title}</h4>
                    <p className="category">{this.props.category}</p>
                </div>
                <div
                    className={
                        "content" +
                        (this.props.ctAllIcons ? " all-icons" : "") +
                        (this.props.ctTableFullWidth ? " table-full-width" : "") +
                        (this.props.ctTableResponsive ? " table-responsive" : "")
                    }
                >
                    {this.props.content}

                    <div className="footer">
                        {this.props.stats != null ? <hr /> : ""}
                        <div className="stats">
                            <i className={this.props.statsIcon} /> {this.props.stats}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ChartWrapper;
