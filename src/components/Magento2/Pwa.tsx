import * as React from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import axios from 'axios';

const magento2GraphQL = axios.create({
    baseURL: 'https://magentoce232pwa.local/graphql',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
});

const GET_DATA_MAGENTO2 = `
{
    products(filter: {price: {gt:"10"}}){
        total_count
        items {
            name
            sku
        }
    }
}
`;

const MAGENTO2_CATEGORY_LIST = `
{
  categoryList(filters: {}) {
    id
    level
    name
    breadcrumbs {
      category_id
      category_name
      category_level
      category_url_key
    }
  }
}
`;

class Pwa extends React.Component<RouteComponentProps> {
    /*constructor(props: RouteComponentProps) {
        super(props);
    }*/

    private fetchAccessToken() {
        axios({
            method: 'POST',
            url: 'https://magentoce232pwa.local/index.php/rest/V1/integration/admin/token',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            data: {
                username: process.env.REACT_APP_MAGENTO_API_USERNAME,
                password: process.env.REACT_APP_MAGENTO_API_PASSWORD,
            }
        }).then(result => {
            console.log(result.data);
        }).catch(error => {
            console.log(error);
        });
    }

    public componentDidMount(): void {
/*
        magento2GraphQL
            .post('', { query: GET_DATA_MAGENTO2 })
            .then(result => {
                console.log(result.data);
            }).catch(error => {
                console.log(error);
            });

*/

        console.log(process.env.REACT_APP_MAGENTO_API_USERNAME);
        this.fetchAccessToken();

        // Send a GET request
        axios({
            method: 'GET',
            url: 'https://magentoce232pwa.local/rest/V1/chart/report-order',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
        }).then(result => {
            console.log(result.data);
        }).catch(error => {
            console.log(error);
        });

        /*
        // Send a GET request
        axios({
            method: 'GET',
            url: 'https://magentoce232pwa.local/index.php/rest/V1/categories',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer uh41qbk8hlvinwe9iq80ldlpg9680t1v',
            }
        }).then(result => {
            console.log(result.data);
        }).catch(error => {
            console.log(error);
        });
*/

        /*fetch('https://magentoce232pwa.local/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({
                query: `{
                            products(filter: {price: {gt:"10"}}){
                                total_count
                                items {
                                    name
                                    sku
                                }
                            }
                        }`,
            }),
        })
            .then(response => {
            return response.json()
        })
            .then(responseAsJson => {
            console.log(responseAsJson);
        })
            .catch(error => {
            console.log(error);
        });


         */

        /*fetch('https://api-stg.smartbuddy.vn/v1/public/products?&searchJoin=and&search=site_id:2;is_active:1&searchFields=site_id:=;is_active:=&status=true&limit=10&page=1&product_family_id=eloj65peo83v8ndy&site_id=2', {
            method: 'GET',
        })
        .then(response => {
            return response.json()
        })
        .then(responseAsJson => {
            console.log(responseAsJson);
        })
        .catch(error => {
            console.log(error);
        });*/

        /*fetch('https://magentoce232pwa.local/index.php/rest/V1/customers/me', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ck539w257zeh765ec2olp43fqe95mukm',
            },
        })
            .then(response => {
                return response.json()
            })
            .then(responseAsJson => {
                console.log(responseAsJson);
            })
            .catch(error => {
                console.log(error);
            });

         */
    }

    public render() {
        console.log(process.env.NODE_ENV);
        console.log(process.env.REACT_APP_APP_ENV);
        return ('');
    }
}

export default withRouter(Pwa)
