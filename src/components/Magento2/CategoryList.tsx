import * as React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Grid, Row, Col, Thumbnail, Button } from "react-bootstrap";

import { makeOptimizedUrl } from '../../util/makeUrl';
import {Link} from "react-router-dom";

import CategoryDetail from "./CategoryDetail";

const getCategoryList = gql`
    query categoryList($id: Int!) {
        category(id: $id) {
            id
            children {
                id
                name
                url_key
                url_path
                children_count
                path
                image
                productImagePreview: products(pageSize: 1) {
                    items {
                        small_image {
                            url
                        }
                    }
                }
            }
        }
    }
`;

const handleUrlResolver = gql`
    query resolveUrl($urlKey: String!) {
        urlResolver(url: $urlKey) {
            type
            id
        }
    }
`;

interface Data {
    category: {
        id: number;
        children: Array<{
            id: number;
            name: string;
            url_key: string;
            url_path: string;
            children_count: number;
            path: string;
            image: string;
            productImagePreview: [];
        }>;
    };
}

interface Variables {
    id: number;
}

interface UrlResolver {
    urlKey: string;
}

const categoryUrlSuffix = '.html';

const previewImageSize = 480;

class CategoryList extends React.Component {

    public getImagePath(image: string, productImagePreview: any) {
        const previewProduct = productImagePreview.items[0];
        if (image) {
            return process.env.REACT_APP_MAGENTO_BACKEND_URL + makeOptimizedUrl(image, {
                type: 'image-category',
                width: previewImageSize,
                height: 80
            });
        } else if (previewProduct) {
            return makeOptimizedUrl(previewProduct.small_image.url, {
                type: 'image-product',
                width: previewImageSize,
                height: 80
            });
        } else {
            return '';
        }
    }

    public render() {
        return (
            <React.Fragment>
                <Query<any, Variables> query={getCategoryList} variables={{ id: 2 }}>
                    {
                        ({ loading, error, data }) => {
                            if (loading) {
                                console.log(loading);

                                return <span>Fetching Data...</span>;
                            }
                            if (error) {
                                console.log(error.message);

                                return (
                                    <span className="">
                                        Data Fetch Error:{' '}
                                        <pre>{error.message}</pre>
                                    </span>
                                );
                            }
                            if (data.category.children.length === 0) {
                                return (
                                    <div className="">
                                        No child categories found.
                                    </div>
                                );
                            }
                            console.log('AAAAAAAAB');
                            console.log(data);

                            const children = data.category.children.sort((a: any, b: any) => {
                                if (a.position > b.position) {
                                    return 1;
                                } else if (a.position === b.position && a.id > b.id) {
                                    return 1;
                                } else {
                                    return -1;
                                }
                            });

                            console.log(children);

                            return (
                                <Grid fluid>
                                    <Row>
                                        <React.Fragment>
                                            {
                                                children.map((node: any) =>
                                                    <Col key={node.id} xs={6} md={3}>
                                                        <Thumbnail src={this.getImagePath(node.image, node.productImagePreview)} alt={node.name}>
                                                            <h3>{node.name}</h3>
                                                            <p>
                                                                <Link
                                                                    to={`categories/${node.url_path}${categoryUrlSuffix}`}
                                                                    title={node.name}
                                                                    className="btn btn-primary"
                                                                >
                                                                    {node.name}
                                                                </Link>
                                                            </p>
                                                        </Thumbnail>
                                                    </Col>
                                                )
                                            }
                                        </React.Fragment>
                                    </Row>
                                </Grid>
                            );
                        }
                    }
                </Query>
            </React.Fragment>
        );
    }
}

export default CategoryList;
