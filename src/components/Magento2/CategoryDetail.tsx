import * as React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {RouteComponentProps} from "react-router-dom";

export interface CategoryDetailProps {
    url: string;
    id: string;
}

const handleUrlResolver = gql`
    query resolveUrl($urlKey: String!) {
        urlResolver(url: $urlKey) {
            type
            id
        }
    }
`;

const CategoryDetail = ({ match }: RouteComponentProps<CategoryDetailProps>) => {

    console.log('you are here !!!!! Category Detail');
    console.log(match.params.id);
    //console.log(props.id);
    const { loading, error, data } = useQuery(handleUrlResolver, {
        variables: { urlKey: match.params.id },
    });

    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;

    console.log(data);

    return (
        <div>

        </div>
    );
};

export default CategoryDetail;
