import * as React from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import axios from 'axios';
import { ApolloProvider, Query } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';
import CategoryList from "./CategoryList";
import CategoryDetail from "./CategoryDetail";

const magento2GraphQL = axios.create({
    baseURL: 'https://magentoce232pwa.local/graphql',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
});

const httpLink = createHttpLink({
    uri: 'https://magentoce232pwa.local/graphql'
});

const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache()
});

const GET_NAVIGATION_MENU = `
{
    category(id: 2) {
        id
        children {
            id
            name
            url_key
            url_path
            children_count
            path
            image
            productImagePreview: products(pageSize: 1) {
                items {
                    small_image {
                        url
                    }
                }
            }
        }
    }
}
`;

const DEMO_QUERY = gql`
  query categoryList($id: Int!) {
    category(id: $id) {
        id
        children {
            id
            name
            url_key
            url_path
            children_count
            path
            image
            productImagePreview: products(pageSize: 1) {
                items {
                    small_image {
                        url
                    }
                }
            }
        }
    }
}
`;

class NavigationMenu extends React.Component {

    public componentDidMount(): void {
        /**magento2GraphQL
            .post('', { query: GET_NAVIGATION_MENU })
            .then(result => {
                console.log(result.data);
            }).catch(error => {
            console.log(error);
        });*/
        //console.log(this.demo());
        //this.demo();
    }

    /*public demo() {
        return (
            <Query query={DEMO_QUERY} variables={{ id: 2 }}>
                {({ loading, error, data }: any) => {
                    console.log(loading);
                    if (loading) return <div>Fetching</div>

                    console.log(error);
                    if (error) return <div>Error</div>

                    console.log(data);
                    return (data);
                }}
            </Query>
        );
    }*/

    public render() {
        return (
            <ApolloProvider client={client}>
                <CategoryList />
            </ApolloProvider>
        );
    }
}

export default NavigationMenu;
