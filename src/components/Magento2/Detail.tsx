import * as React from 'react';
import {Link, RouteComponentProps, withRouter} from 'react-router-dom';
import {ApolloProvider, Query} from "react-apollo";
import gql from 'graphql-tag';
import {Col, Grid, Row, Thumbnail} from "react-bootstrap";
import {createHttpLink} from "apollo-link-http";
import {ApolloClient} from "apollo-client";
import {InMemoryCache} from "apollo-cache-inmemory";

interface Variables {
    urlKey: string;
}

interface CategoryVariables {
    id: number;
    pageSize: number;
    currentPage: number;
    onServer: boolean;
    idString: string;
}

interface DetailProps {
    id: string;
}

interface ComponentProps extends RouteComponentProps<DetailProps> {}

const handleUrlResolver = gql`
    query resolveUrl($urlKey: String!) {
        urlResolver(url: $urlKey) {
            type
            id
        }
    }
`;

const categoryQuery = gql`
    query category(
        $id: Int!
        $pageSize: Int!
        $currentPage: Int!
        $onServer: Boolean!
        $idString: String
    ) {
        category(id: $id) {
            id
            description
            name
            product_count
            meta_title @include(if: $onServer)
            meta_keywords @include(if: $onServer)
            meta_description @include(if: $onServer)
        }
        products(
            pageSize: $pageSize
            currentPage: $currentPage
            filter: { category_id: { eq: $idString } }
        ) {
            filters {
                name
                filter_items_count
                request_var
                filter_items {
                    label
                    value_string
                }
            }
            items {
                id
                name
                small_image {
                    url
                }
                url_key
                price {
                    regularPrice {
                        amount {
                            value
                            currency
                        }
                    }
                }
            }
            page_info {
                total_pages
            }
            total_count
        }
    }
`;

const httpLink = createHttpLink({
    uri: 'https://magentoce232pwa.local/graphql'
});

const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache()
});

class Detail extends React.Component<ComponentProps> {

    public static defaultProps: Partial<DetailProps> = {
        id: '',
    };

    public render() {
        console.log('you are here !!!!!');
        console.log(this.props.match.params.id);
        return (
            <ApolloProvider client={client}>
                <React.Fragment>
                    <Query<any, CategoryVariables> query={categoryQuery} variables={
                        {
                            id: 3,
                            pageSize: 6,
                            currentPage: 1,
                            onServer: false,
                            idString: String(3)
                        }
                    }>
                        {
                            ({ loading, error, data }) => {
                                if (loading) {
                                    console.log(loading);

                                    return <span>Fetching Data...</span>;
                                }
                                if (error) {
                                    console.log(error.message);

                                    return (
                                        <span className="">
                                            Data Fetch Error:{' '}
                                            <pre>{error.message}</pre>
                                        </span>
                                    );
                                }
                                console.log('Category Detail');
                                console.log(data);

                                return (
                                    <React.Fragment>
                                    </React.Fragment>
                                );
                            }
                        }
                    </Query>
                </React.Fragment>
            </ApolloProvider>
        );
    }
}

export default Detail;
