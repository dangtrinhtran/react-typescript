import * as React from 'react';
import { NavLink } from "react-router-dom";

import NavBarsLinks from "../NavBars/NavBarsLinks";

import logo from "../../assets/img/reactlogo.png";

interface SideBarProps {
    image: string;
    color: string;
    hasImage: boolean;
    routes: any;
    location: any;
}

interface SideBarState {
    width: number;
}

class SideBar extends React.Component<SideBarProps, SideBarState> {

    constructor(props: SideBarProps) {
        super(props);
        this.state = {
            width: window.innerWidth,
        };
    }

    public static defaultProps: Partial<SideBarProps> = {
        image: '',
        routes: '',
        color: '',
        hasImage: false,
    };

    public activeRoute(routeName: string): string {
        return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
    }

    public updateDimensions(): void {
        this.setState({ width: window.innerWidth });
    }

    public componentDidMount(): void {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions.bind(this));
    }

    public render() {
        const sidebarBackground = {
            backgroundImage: "url(" + this.props.image + ")"
        };
        return (
            <div
                id="sidebar"
                className="sidebar"
                data-color={this.props.color}
                data-image={this.props.image}
            >
                {this.props.hasImage ? (
                    <div className="sidebar-background" style={sidebarBackground} />
                ) : (
                    null
                )}
                <div className="logo">
                    <a
                        href="#"
                        className="simple-text logo-mini"
                    >
                        <div className="logo-img">
                            <img src={logo} alt="logo_image" />
                        </div>
                    </a>
                    <a
                        href="#"
                        className="simple-text logo-normal"
                    >
                        React
                    </a>
                </div>
                <div className="sidebar-wrapper">
                    <ul className="nav">
                        {this.state.width <= 991 ? <NavBarsLinks /> : null}
                        {this.props.routes.map((prop: any, key: any) => {
                            if (!prop.redirect)
                                return (
                                    <li
                                        className={
                                            prop.upgrade
                                                ? 'active active-pro'
                                                : this.activeRoute(prop.layout + prop.path)
                                        }
                                        key={key}
                                    >
                                        <NavLink
                                            to={prop.layout + prop.path}
                                            className="nav-link"
                                            activeClassName="active"
                                        >
                                            <i className={prop.icon} />
                                            <p>{prop.name}</p>
                                        </NavLink>
                                    </li>
                                );
                            return null;
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}

export default SideBar;
