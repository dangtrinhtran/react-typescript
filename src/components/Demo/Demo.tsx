import * as React from 'react';
import {RouteComponentProps, withRouter} from 'react-router-dom';
import DemoProps from "./DemoProps";

class Demo extends React.Component<RouteComponentProps<any>> {

    public render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <DemoProps demoTest={10} />
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Demo)
