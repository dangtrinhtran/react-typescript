import * as React from 'react';

interface MyProps {
    demoTest?: number;
}

interface MyState {
    demo: number;
}

class DemoProps extends React.Component<MyProps, MyState> {

    public static defaultProps: Partial<MyProps> = {
        demoTest: 1,
    };

    public state: MyState = {
        demo: 0,
    };

    public testFunction = () => {
        const checkProp: number = this.props.demoTest!;
        const checkDemo = this.state.demo + checkProp;
        this.setState({
            demo: checkDemo,
        });
    };

    public render() {
        //console.log(this.state.demo);
        //console.log(this.testFunction());
        return (
            <div>
                <button className="button" onClick={this.testFunction}>Demo: {this.state.demo}</button>
            </div>
        );
    }
}

export default DemoProps;
