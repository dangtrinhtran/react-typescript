import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import axios from 'axios';


interface IState {
    error: any;
    isLoaded: boolean;
    customers: any[];
}

export default class List extends React.Component<RouteComponentProps, IState> {
    constructor(props: RouteComponentProps) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            customers: []
        };
    }

    public componentDidMount(): void {
        /*axios.get(`http://localhost:5001/customers`).then(data => {
            this.setState({ customers: data.data });
        });*/
        fetch('http://localhost:5001/customers')
            .then(results => {
                return results.json();
            }).then(data => {
                console.log(data);
                this.setState({
                    customers: data,
                    isLoaded: true
                });
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    error,
                    isLoaded: false
                });
            });

    }

    public deleteCustomer(id: number) {
        axios.delete(`http://localhost:5001/customers/${id}`).then(data => {
            const index = this.state.customers.findIndex(customer => customer.id === id);
            this.state.customers.splice(index, 1);
            this.props.history.push('/');
        });
    }

    public render() {
        const customers = this.state.customers;
        console.log(process.env.NODE_ENV);
        console.log(process.env.REACT_APP_APP_ENV);
        return (
            <div>
                {customers.length === 0 && (
                    <div className="text-center">
                        <h2>No customer found at the moment</h2>
                    </div>
                )}

                <div className="container">
                    <div className="row">
                        <table className="table table-bordered">
                            <thead className="thead-light">
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Address</th>
                                <th scope="col">Description</th>
                                <th scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {customers && customers.map(customer =>
                                <tr key={customer.id}>
                                    <td>{customer.first_name}</td>
                                    <td>{customer.last_name}</td>
                                    <td>{customer.email}</td>
                                    <td>{customer.phone}</td>
                                    <td>{customer.address}</td>
                                    <td>{customer.description}</td>
                                    <td>
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group" style={{ marginBottom: "20px" }}>
                                                <Link to={`edit/${customer.id}`} className="btn btn-sm btn-outline-secondary">Edit Customer </Link>
                                                <button className="btn btn-sm btn-outline-secondary" onClick={() => this.deleteCustomer(customer.id)}>Delete Customer</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }
}
