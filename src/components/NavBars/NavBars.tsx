import * as React from 'react';
import { Navbar } from "react-bootstrap";

import NavBarsLinks from "./NavBarsLinks";

interface Nav {
    sidebarExists: boolean,
}

interface NavBarsProps {
    brandText: string;
}

class NavBars extends React.Component<NavBarsProps, Nav> {

    constructor(props: NavBarsProps) {
        super(props);
        this.state = {
            sidebarExists: false
        };
    }

    public static defaultProps: Partial<NavBarsProps> = {
        brandText: '',
    };

    public render() {
        return (
            <Navbar fluid>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#pablo">{this.props.brandText}</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Navbar.Collapse>
                    <NavBarsLinks />
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default NavBars;
